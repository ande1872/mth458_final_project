import numpy as np
class Vectors:
	def __init__( this ):
		this.phi = 0.0
		
	def find_Phi( this, x, y):
		this.dot = np.dot(x,y)
		this.x_norm = np.sqrt(np.sum(np.multiply(x,x)))
		this.y_norm = np.sqrt(np.sum(np.multiply(y,y)))
		
		this.phi = np.arccos(np.divide(this.dot, np.multiply(this.x_norm, this.y_norm) ))
		
		return this.phi
		
	def get_phi(this):
		return this.phi 
		
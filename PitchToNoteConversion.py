import math
from math import log2, pow
import copy
import numpy as np
 
class PTNC:
	def __init__(this):
		this.A4    = 432 # Hz
		this.Scale = 12
		# Base Reference point to begin our scaling from
		this.C0    = this.A4 * ( 2**(-4.75) )
		
		#this.map_to_letter = { 0: "B#/C/Dbb",
		#					   1: "C#/Db",
		#					   2: "C##/D/Ebb",
		#					   3: "D#/Eb",
		#					   4: "D##/E/Fb",
		#					   5: "F/E#",
		#					   6: "F#/Gb",
		#					   7: "F##/G/Abb",
		#					   8: "G#/Ab",
		#					   9: "G##/A/Bbb",
		#					  10: "A#/Bb",
		#					  11: "A##/B/Cb" 				
		#					}
		this.map_to_letter = { 0: "C", # 1   [1, 17, 9, 150, 5, 170, 180, 3, 100, 210, 230, 240] # Practice Hz to make sure the counter is working correctly
							   1: "C#",    # 17
							   2: "D", # 9
							   3: "D#",     # 150
							   4: "E", # 5
							   5: "F",    # 170
							   6: "F#",  # 180
							   7: "G", # 3
							   8: "G#",     # 100
							   9: "A", # 210
							  10: "A#",     # 230
							  11: "B"  # 240			
							}
	# pitches in an array of pitches seen from the fft output
	def find_steps(this,pitches):
		this.steps = []
		for pitch in pitches:
			this.half_tones = round( this.Scale * log2( pitch / this.C0) )
			this.step = this.half_tones % this.Scale
			this.steps.append(this.step)
	
		return this.steps
		
	def find_octaves(this,pitches):
		this.octaves = []
		for pitch in pitches:
			this.half_tones = round( this.Scale * log2( pitch / this.C0) )
			this.ocatve = this.half_tones // this.Scale
	
		return this.octave
			
			
	def remove_duplicates(this, array): 
		this.final_list = [] 
		for note in array: 
			if note not in this.final_list: 
				this.final_list.append(note) 
		return this.final_list
    	 		
	def find_notes(this,steps):
		this.notes = []
		for step in steps:
			this.notes.append(this.map_to_letter[step])
		this.notes = this.remove_duplicates(this.notes)
		return this.notes
		


	def recursion_update(pos, dictionary):
		temp = []
		for l in range(len(dictionary)):
			temp.append(pos)
			
		
	
	# This function returns an array of arrays that contain all the known 
	# path sequences for this particular instance of notes seen from a 
	# FFT or Short Time Fourier Transform output.
	def return_array_of_possible_note_paths(this, list_of_notes):
		this.hash_array = []
		for notes in list_of_notes:
			this.split = notes.split("/")
			this.has = {}
			for note in this.split:
				this.has[note] = None
			this.hash_array.append(this.has)
		
		# Looks silly, but this will build a binary tree of the inputted pitches seen
		# from one instance of the STFT/FTT output.
		for i in range(0, len(this.hash_array)):
			if len(this.hash_array) - 2 - i < 0:
				break
			for key in list(this.hash_array[len(this.hash_array) - 2 - i].keys()):
				this.hash_array[len(this.hash_array) - 2 - i][key] = this.hash_array[len(this.hash_array) - 1 - i]
				print(key)
		
		# Then this bit takes the tree just made and walks it out to produce an array
		# of arrays that will be used in the count_durations below.
		this.array_of_note_sequences = []
		total_arrays_needed = len(this.hash_array)
		for i in range(len(this.hash_array)):
			total_arrays_needed *= len(this.hash_array[i])
		
		for i in range(total_arrays_needed):
			this.array_of_note_sequences.append([])	
		
		for l in range(len(this.hash_array)):

			keys = list(this.hash_array[l].keys())
			shift = int(len(this.array_of_note_sequences) / len(keys))
			for i in range(shift):
				if len(keys) == 2:
					this.array_of_note_sequences[i].append(keys[0])
					this.array_of_note_sequences[i + shift].append(keys[1])
				elif len(keys) == 3:
					this.array_of_note_sequences[i].append(keys[0])
					this.array_of_note_sequences[i + shift].append(keys[1])
					this.array_of_note_sequences[i + (shift*2)].append(keys[2])
				
			
		#for key in list(this.hash_array[0].keys()):
		#	temp_list = []
		#	temp = []
		#	temp.append(key)
		#	for i in range(1,len(this.hash_array)):
		#		key_list = list(this.hash_array[i].keys())
		#		for l in range(len(this.hash_array[i])):
		#			temp_list.append(temp)
		#			temp_list[len(temp_list) - 1].append(key_list[l])	
		#		print(temp_list)
		#		
		#		for key in list(this.hash_array[i].keys()):
		#								
		#			temp_list.append(temp)
		#			
		#			copy_ = copy.deepcopy(temp)
		#			copy_.append(this.list[l])
		#			temp_list.append(copy_)
		#			
		#	this.array_of_note_sequences = this.array_of_note_sequences + temp_list
			
		return this.array_of_note_sequences
					
	# Only updates the counts of each of the notes and if the note is no longer seen
	# then we delete it from the hash table. Point is to get to the end of the tree
	# and then use the notes seen and the durations derived from the counts of the 
	# Hash table.
	def updateCount(this, hahs, note):
		for key in list(hahs.keys()):	
			#if key in notes:
			if key == note:
				hahs[key] += 1
			else:
				hahs.pop(key)
		#for note in notes:
		if note not in list(hahs.keys()):
			hahs[note] = 1 
		return hahs
			

	def CountRecursion(this, hahs, buffre):
		# temp = copy.deepcopy(array[1])
		temp = []
		print(hahs, "\n")
		#for i in range( 1, len(buffre)):
		if len(buffre) >= 1:
			for l in range(len(buffre[0])):
				temp.append( this.updateCount( hahs, buffre[0][l] ) )
			
			temp = temp + this.CountRecursion( temp[len(temp) - 1], buffre[1:])
		
		return temp
				
		
	# Takes a array of an array of note sequences so, that it may start at the first one and 
	# count all the possible times a certain note is seen. Keeps duration values as it moves through
	# the buffer of pitches seen from our stochastic sequence.
	def count_duration(this, buffer_of_possible_paths):
		this.note_duration_hash_buffer = []	
		#if len(buffer_of_possible_paths) == 1:
		#	this.note_duration_hash = {}
		#	for note in buffer_of_possible_paths[0]:
		#		this.note_duration_hash
		#else:
		iteration_hash = {}
		
		for z in range(len(buffer_of_possible_paths)):

			# Create the initial Hash Table to begin counting our sequence from.
			note_duration_hash = {}			
			for note in (buffer_of_possible_paths[z]):

				note_duration_hash[note] = 1
			
			iteration_hash[z] = None

			# Recursively iterate through all the possibilities.
			if len(buffer_of_possible_paths) >= 2:
				iteration_hash[z] = this.CountRecursion( note_duration_hash, buffer_of_possible_paths[1:] )
			else:
				iteration_hash[z] = note_duration_hash
			
		return iteration_hash
	
from collections import Counter
	
# Testing the class to make sure everything works accordingly.
if __name__ == '__main__':
	
	# Generate a class instance of PitchToNoteConversion
	ptnc = PTNC()
	print("success", ptnc)
	
	# Example pitches
	#pitches  = [100, 1000, 2000, 4000, 6000, 245, 3456] # Pitches here being the output of a Transform, usually FFT.
	pitches = [1, 17, 9, 150, 5, 170, 180, 3, 100, 210, 230, 240]
	pitches2 = np.multiply( 2, np.array(pitches))			# Second point of stft output as desired.
	pitches3 = np.add( 300, pitches2)
	pitches4 = np.subtract( pitches3, 400)
	
	
	steps = ptnc.find_steps(pitches)
	print("Steps Found", steps)
	
	notes = ptnc.find_notes(steps)
	print("Notes Found", notes)
	
	# Work in progress, was attempting to walk the entire Binary tree being made, but had memory issues.
	#possible_paths = ptnc.return_array_of_possible_note_paths(notes)
	#print("Total Possible Note combinations are", len(possible_paths), ".\n")

	buffer_of_pitch_paths = []
	#buffer_of_pitch_paths.append( ptnc.return_array_of_possible_note_paths( ptnc.find_notes( ptnc.find_steps( pitches  ) ) ) )
	#buffer_of_pitch_paths.append( ptnc.return_array_of_possible_note_paths( ptnc.find_notes( ptnc.find_steps( pitches2 ) ) ) )
	#buffer_of_pitch_paths.append( ptnc.return_array_of_possible_note_paths( ptnc.find_notes( ptnc.find_steps( pitches3 ) ) ) )
	#buffer_of_pitch_paths.append( ptnc.return_array_of_possible_note_paths( ptnc.find_notes( ptnc.find_steps( pitches4 ) ) ) )
	for i in range(0,50):
		#buffer_of_pitch_paths.append( ptnc.return_array_of_possible_note_paths( ptnc.find_notes( ptnc.find_steps( pitches  ) ) ) )
		buffer_of_pitch_paths += ( ptnc.find_notes( ptnc.find_steps( pitches  ) ) )
		
		counts = Counter(buffer_of_pitch_paths )
		print(counts)
	

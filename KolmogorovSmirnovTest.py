from scipy import stats
import numpy as np

class KomogorovSmirnovTest:
	def __init__(this):
		this.kst = stats.kstest
		this.dist_continu  = [d for d in dir(stats) if isinstance(getattr(stats, d), stats.rv_continuous)]
		this.distribution_error = []
				
	def find_distribution_errors(this, data):
		for distribution in this.dist_continu:
			this.distribution_error.append(this.kst(data, cdf=distribution))	
			
		return np.array( this.distribution_error )
		
		
	def get_distributions(this):
		return np.array(this.dist_continu)
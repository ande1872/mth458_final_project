# Now for FFT Analysis of the sequence at hand, then average of the sequence to find 
# local maxima for use in Dr. Chew's Model
from scipy.fftpack import fft, fftfreq, rfft
from scipy.io.wavfile import read
from scipy import signal
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

fname = 'StochasticSequenceDataFile.csv.wav'
fs, data = read(fname)

print("Performing FFT Analysis")
sampleRate = 1
f, time, Zxx = signal.stft(data, fs=sampleRate, nperseg=256)
#print(f, len(f[0:100]), f[0], f[100])
#print()
#print(time, len(time), "\n")
#print(len(Zxx))
#print(f[0], Zxx[0][0], f[1], np.abs(Zxx[1][0]), f[2], Zxx[2][0], f[3], Zxx[3][0],f[4], Zxx[4][0])
# Modifies the range of frequencies we are looking at and plotting. Higher frequencies over 
# half the sample rate, which was 1 second => Frequencies over 0.5 are unusable.
f = f[0:4]
Zxx = Zxx[0:4]
plt.pcolormesh(time, f, np.abs(Zxx))
plt.title("STFT Magnitude")
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time')
plt.show()

# Custom libraries
from CenterEffectGenerator import CEG 
from PitchToNoteConversion import PTNC
from collections import Counter
from  KolmogorovSmirnovTest import KomogorovSmirnovTest
from vector_math import Vectors

# Used to determine the notes and durations of them for use with 
# Dr. Chew's Model below.
ptnc = PTNC()

# Convert the pitches provided so that I may apply a hash table later on 
# to figure out the most likely pattern to follow.
Array_of_Freqs = []
print("Finding Frequencies that have a magnitude greater than 2.")
for t in range(0,len(time)):
	freqs = []
	# This is vetting the frequencies seen from our sequence and making sure 
	# they are over a certain amount in magnitude. This could also be some sort of
	# averaging between relative frequencies to make sure that I have a local maximum
	for freq in range(0,len(f)):
		if freq == 0:
			continue 
		elif np.abs(Zxx[freq][t]) > 2.0:
			freqs.append(f[freq])
	Array_of_Freqs.append(freqs)

Array_of_Notes = []
Notes     = [] # This will be a nxm matrix, where n is time count and m is frequencies at that time.
Durations = [] # This will be a nxm matrix, where n is time count and m is the duration of that frequency at that time
print("Counting durations of the frequencies seen over the course of the programs execution")
for i in range(len(Array_of_Freqs)):
	print("On ", i, " out of ", len(Array_of_Freqs), end="\r")
	
	Array_of_Notes += ( ptnc.find_notes( ptnc.find_steps(Array_of_Freqs[i]) ) )
	counts = Counter(Array_of_Notes)
	durations = {}
	for key in list(counts.keys()):
		durations[key] = counts[key] / (i + 1) 

	temp_Notes = []
	temp_Durations = []
	for key in list(durations.keys()):
		temp_Notes.append(key)
		temp_Durations.append(durations[key])
		
	Notes.append(temp_Notes)
	Durations.append(temp_Durations)

# just a new line
print()

# Create instance of Chew's Model and set buffer size of weighted positions to 
# the size of time
ceg = CEG(len(time)) 
# Generate the Spiral Array Model Positions for the Notes to be represented by.
ceg.pitches2(-8,12)
# Generate Key Tonal Centers
ceg.tonal_c2()

wtd_pos = []
print("Finding the Tonal Center of our stochastic sequence")
print("BestGuess            : 2nd                  : 3rd ")
# Produce a vector output of guesses for pitch tonality based on error from 
# the tonal centers created from tonal_c2() 
for i in range(len(Notes)):
	ceg.position2( Notes[i] )
	# Produce a output of weighted positions in Chew's Model based on the notes and durations seen up to this point
	# in time.
	wtd_pos.append( ceg.wtd_pos(Notes[i], Durations[i]) )
	sortedRank, RankValues = ceg.rank_near2()
	print( sortedRank[0] + "(", RankValues[0], ") : " + sortedRank[1] + "(", RankValues[1], ") : " + sortedRank[2] + "(", RankValues[2], ")", end="\r")
	


phis_array = [] 
# Create Vectors class from Vector_math.py
vectors = Vectors()
for i in range(1, len(wtd_pos)):
	# Take orthogonality measurements between subsequent phi values.
	phis_array.append(vectors.find_Phi(wtd_pos[i-1], wtd_pos[i]))
	
# Create KolmogorovSmirnovTest Class to find distribution to use.
kst = KomogorovSmirnovTest()
errors = kst.find_distribution_errors(phis_array)

print("Order of rank for Distribution to Use")
ranked = kst.get_distributions()[errors.argsort()]
print(ranked)
 
# The point of this script is to search through a large space of possible choices when interpreting our stochastic signals.
###########################################################################################################################

##########################################################################################################################

##########################################################################################################################

##########################################################################################################################

##########################################################################################################################
import math # for sqrt function and natural log
from datetime import datetime
import pandas as pd

# This program is expecting a two column input where the date is the first column and
# the second column is the actual stochastic sequence in general. You might need to modify
# the datetime.strptime command below, line 40, to  match your sequence's timestamp.
fname = "StochasticSequenceDataFile.csv"

# Read in the rates provided from our source
with open(fname) as f:
    content = f.readlines()

# Strip off any newline and white space characters
content = [x.strip() for x in content]

# create a hash_table for keeping tally of relative frequencies seen from our observing sequence.
hash_table = {}

hash_table['Rates'] = {"TotalCount": 0} # with the following keys being added to it as they appear unique.
										# {Rate Value 1: Count, ..., Rate Value n: Count}
rates_array  = []
return_array = []
# importing all the possible values and not allowing for any duplicates with 
# hashtable, so for each row in our csv file of rates provided.
for c in content:
	# Remove the comma so we don't get errors later converting the sequence to datetimes
	# and floats 
	array = c.strip().split(",")
	date = datetime.strptime(array[0].strip(), "%Y-%m-%d" )
	
	# This is standardized later in the script and is used to find the time differential 
	# differential in days.
	if array[1] not in ['ND']:
		rates_array.append( [ date, float(array[1]) ] )
	else:
		rates_array.append([ date, 0.0 ])
		
	if len(rates_array) >= 2:
		if rates_array[len(rates_array) - 2][1] != 0.0 and rates_array[len(rates_array) - 1][1] != 0.0:
			return_array.append( [ date, math.log(rates_array[len(rates_array) - 1][1] / rates_array[len(rates_array) - 2][1]) ] )
		
	# Then see if we have the rate already recorded as a unique key in our 
	# hash_table of rates
	if array[1] in hash_table['Rates'].keys(): 
		# if so, then add one to its count 
		hash_table['Rates'][ array[1] ] += 1 
	else:
		# make a new unique entry for it
		hash_table['Rates'][ array[1] ] = 1
	# Followed with adding one to total of all rates we see, which could go 
	# towards infinity.
	hash_table['Rates']["TotalCount"] += 1

keys = hash_table.keys()


# finding relative probabilities of values seen in the sequence

for ky in hash_table['Rates'].keys():
	if ky=="TotalCount":
		continue
	else:
		hash_table['Rates'][ky] = float(hash_table['Rates'][ky]) / float(hash_table['Rates']["TotalCount"])
    
    #print(hash_table[key])


# Now to find the expectation and variance of the EUR/USD exchange rate
# and output into a nifty array

stats = {"Expectation": None, "Variance": None}
for key in keys:
    expectation_sum = 0.0
    variation_sum = 0.0
    k2 = hash_table[key].keys()
    for ky in k2:
    	  # skip what we don't need to pay attention to
        if ky == "TotalCount" or str(ky) in ["N/A ", "N/A", "ND"]:
        	continue
        else:
            expectation_sum += float(ky) * hash_table['Rates'][ky]
            variation_sum   += float(ky) * float(ky)*hash_table[key][ky]
    # now find the variance
    variation_sum = variation_sum - (expectation_sum*expectation_sum)
    stats['Expectation'] = expectation_sum
    stats['Variance']    = variation_sum
    
print("       Expectation: ", stats['Expectation'])
print("          Variance: ", stats['Variance'])
##########################################################################################################################

##########################################################################################################################

##########################################################################################################################

##########################################################################################################################

##########################################################################################################################



# Now we find the normalized version of our data sequence in question by utilizing the 
# expectation and variance just found. 

# stores the value of our original sequence between -1 and 1 
standardized = [] 

stats['Standard Deviation'] = math.sqrt(stats['Variance'])
print("Standard Deviation: ", stats['Standard Deviation'])

for event in rates_array:
	date = event[0]
	rate = event[1]

	if str(rate) in ['0', '0.0']:
		standardized.append([date, rate])
	else:
		standardized.append( [ date,  ( rate - stats['Expectation'] ) / stats['Standard Deviation'] ] )			
			



# Now to shrink this sequence to fit within -1 to 1 for writing out to a mp3 file below.
from functools import reduce
def mapToRange(values):
	
	# Reducing the values to find the max and min values found within the sequence 
	Maximum = 0
	Minimum = float('inf')
	for value in values:

		if Maximum < value[1]:
			Maximum = value[1]
		if Minimum > value[1]:
			Minimum = value[1]
	# Squeeze the values presented to be in between -1 and 1 truthfully, in case we do not have 
	# values that lie already in between -1 and 1.
	pShrink =  1.0 / Maximum
	nShrink =  -1.0 / Minimum

	for i in range(len(values)):
		if values[i][1] == 0:
			continue
		elif values[i][1] > 0:
			values[i][1] = values[i][1] * pShrink
		else:
			values[i][1] = values[i][1] * nShrink

	return values;
	

normalRV = mapToRange(standardized)

##########################################################################################################################

##########################################################################################################################

##########################################################################################################################

##########################################################################################################################

# Now I need to figure out the scaling factor for the dataset in question, so it may be reviewed.


import numpy as np
def scale(args):
	rt = []
	i     = args[0]
	a = args[1]
	b = args[2]
	step  = args[3]
	temp  = -math.pi / 2.0
	#rt.append(array[i][1])
	rt.append(a)
	diff = None
	#if array[i - 1][1] < array[i][1]:
	if a.iloc[0] < b.iloc[0]:
		#diff = array[i-1][1] - array[i][1]
		diff = b - a
	else:
		#diff = array[i][1] - array[i-1][1]
		diff = a - b
	while temp < (math.pi/2.0):
		rt.append(
						rt[len(rt) - 1] + diff * math.sin( (temp + 1.0) / 2.0)
					 )
		temp += step
	return rt 
import multiprocessing # for pooling of this scale function.
import wave, struct
fname2 = fname + '.wav'
sampleRate = 44100 # hertz, samples / second at the given sampleWidth measured in bytes.

def create_wave(step, pd, fname):
	fd = wave.open(fname, "w")

	fd.setnchannels(1) # mono
	fd.setsampwidth(2) # 2 bytes per frame, 1 short integer per frame
	fd.setframerate(sampleRate)

	p = multiprocessing.Pool(multiprocessing.cpu_count())
	# need to pass in an array that has the array being indexed
	step = 10000
	current = 0
	print("Mapping the scaling function now, give me a second.")
	while current < pd.shape[0]:
		arguments = None
		if current + step > pd.shape[0]:
			arguments = [ [x,pd.iloc[x-1], pd.iloc[x], step] for x in range(current,current + (pd.shape[0] - current) )]		
		else:
			arguments = [ [x,pd.iloc[x-1], pd.iloc[x], step] for x in range(current,current + step)]
		current += step
		print("On ", current, " out of ", pd.shape[0])
		scaled_array = p.map(scale, arguments)	

		for scaled in scaled_array:
			scaled = np.int16( scaled/ np.max(np.abs(scaled)) * 32767 )
			for value in scaled:
				data = struct.pack('<h', value)
				fd.writeframesraw(data)
	print("that took longer than a second")
	#for i in range(1, pd.shape[0]):
	#	print("Scaling ", i, " out of ", pd.shape[0])
	#	rt = scale([i, pd.iloc[i-1], pd.iloc[i], step])
		# write out this buffer to the wave file and continue our process.
	#	scaled = np.int16( rt/np.max(np.abs(rt)) * 32767 )
	#	for value in scaled:		
	#		data = struct.pack('<h', value)
	#		fd.writeframesraw(data)
	
	#fd.writeframes('')
	fd.close()
			
# Scale the original sequence into 20 second intervals
# and interpolate the new sequence using the timestamps 
# attached to each rate.
pd.set_option('precision',12)
normalrv_df = pd.DataFrame(normalRV, columns=['date', 'rate'])
normalrv_df = normalrv_df.set_index('date')
upsampled   = normalrv_df.resample("1S")

interpolated = upsampled.interpolate(method='time')
print(interpolated.tail(), interpolated.info())	
print("Shape of interpolated values: ", interpolated.shape)				


print("great Success")


##########################################################################################################################

import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import write
plt.figure()
# Data is an array of values that exist between -1 and 1,
# Hence we utilize the two functions provided from my CEG.js script to 
# 	1st: up-convert the original sequence and then 
# 	2nd: scale to integer values
#   3rd: write the function out

# 1st step
# data = np.random.uniform(-1,1,44100)
rates_df = pd.DataFrame(rates_array, columns=['date', 'rate'])
rates_df = rates_df.set_index('date')

plt.plot(interpolated.index.values, interpolated['rate'], 
		 rates_df.index.values, rates_df['rate'])
plt.legend(['Normalized Interpolated 1 Second Rates', 'Original Fed Fund Rates'])

# Plotting out the returns for the EUR/USD Exchange Pair
plt.figure()
returns_df = pd.DataFrame(return_array, columns=['date', 'return'])
returns_df = returns_df.set_index('date')
plt.plot(returns_df.index.values, returns_df['return'])
plt.legend(['Log Return Fed Fund Rates'])
plt.show()

########################################################################
# 2nd step
# scaled = np.int16(data/np.max(np.abs(data)) * 32767)
scaled = np.int16(interpolated/np.max(np.abs(interpolated)) * 32767)
interpolated = None
import gc
gc.collect()


########################################################################
# 3rd step
sampleRate_of_wav_file = 1 #
fname3 =  fname + '.wav'
write(fname3, sampleRate_of_wav_file, scaled) # interpolated['rate'].values)
print("Done writing out wave file")


#############################################################################################
#############################################################################################
# Now to write out the returns as a wave file for STFT Analysis

#upsampled   = returns_df.resample("1S")

#interpolated = upsampled.interpolate(method='time')

#scaled = np.int16(interpolated/np.max(np.abs(interpolated)) * 32767)

#sampleRate_of_wav_file = 1 #
#fname = 'EUR_USD_Day_Log_Return.wav'
#write(fname, sampleRate_of_wav_file, scaled) # interpolated['rate'].values)
print("Done writing out wave file")